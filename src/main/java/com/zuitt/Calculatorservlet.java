package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Calculatorservlet extends HttpServlet {

	
	private static final long serialVersionUID = 5412752341120568755L;
	
	public void init() throws ServletException{
		System.out.println("*************");
		System.out.println("Initialized connection database");
		System.out.println("*************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));	
		int result = 0;
		String operation = req.getParameter("operation");
		
		if (operation.equals("add")) {
		result = num1 + num2;
		}
		
		else if (operation.equals("subtract")) {
			result = num1 - num2;
			}
		
		else if (operation.equals("multiply")) {
			result = num1 * num2;
			}
		
		else if (operation.equals("divide")) {
			result = num1 / num2;
			}
		
		
		PrintWriter out = res.getWriter();
		
	
		
		out.println("<h1> The two numbers you provided are: " + num1 + "," + num2  );
		out.println("<h2> The operation that you wanted is: " + operation + "<br>" );
		out.println("<h2> The result is: " + result + "<br>"  );
		
		
		
		
		
	}
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app.</h1>");
		out.println("<h2>To use the app, input two numbers and an operation<br><br>");
		out.println("Hit the submit button after filling the details<br><br>");
		out.println("You will get the result shown in your browser!");
		
	}
	
	public void destroy() {
		System.out.println("*************");
		System.out.println("Destroyed CalculatorServlet");
		System.out.println("*************");
	}
	
}
